// Global scope variables
let undoStack = [];
let redoStack = [];
let brushSize;
let drawing = false;
let currentShape = '';
let currentColor;
let startX;
let startY;
let isDrawing = false;

const canvas = document.getElementById('drawingCanvas');
const ctx = canvas.getContext('2d');

const imageLoader = document.getElementById('imageLoader');
imageLoader.addEventListener('change', handleImage, false);

function handleImage(e) {
  const reader = new FileReader();
  reader.onload = function(event) {
    const img = new Image();
    img.onload = function() {
      ctx.drawImage(img, 0, 0, canvas.width, canvas.height);
    }
    img.src = event.target.result;
  }
  reader.readAsDataURL(e.target.files[0]);
}

document.addEventListener("DOMContentLoaded", function() {
  currentColor = document.getElementById('colorPicker').value;
  brushSize = document.getElementById('brushSize').value;
  attachEventListeners();
});

function attachEventListeners() {
  const colorPicker = document.getElementById('colorPicker');
  colorPicker.addEventListener('change', function() {
    currentColor = this.value;
  });

  const eraserButton = document.getElementById('eraserButton');
  eraserButton.addEventListener('click', function() {
    currentColor = '#FFFFFF';
  });

  const clearButton = document.getElementById('clearButton');
  clearButton.addEventListener('click', function() {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
  });

  const brushSizeInput = document.getElementById('brushSize');
  brushSizeInput.addEventListener('input', function() {
    brushSize = this.value;
  });

  document.getElementById('saveButton').addEventListener('click', function() {
    const dataURL = canvas.toDataURL('image/png');
    const link = document.createElement('a');
    link.href = dataURL;
    link.download = 'my_drawing.png';
    link.click();
  });

  document.getElementById('brushButton').addEventListener('click', function() {
    currentShape = 'brush';
  });

  document.getElementById('freehandButton').addEventListener('click', function() {
    currentShape = 'freehand';
  });

  document.getElementById('undoButton').addEventListener('click', undoLastAction);
  document.getElementById('redoButton').addEventListener('click', redoLastAction);

  canvas.addEventListener('mousedown', startDrawing);
  canvas.addEventListener('mousemove', draw);
  canvas.addEventListener('mouseup', endDrawing);
  canvas.addEventListener('mouseout', endDrawing);
}


function startDrawing(e) {
  isDrawing = true;
  let rect = canvas.getBoundingClientRect();
  startX = e.clientX - rect.left;
  startY = e.clientY - rect.top;
  if (currentShape === 'freehand') {
    ctx.beginPath();
    ctx.moveTo(startX, startY);
  }
}

function draw(e) {
  if (!isDrawing) return;
  let rect = canvas.getBoundingClientRect();
  let currentX = e.clientX - rect.left;
  let currentY = e.clientY - rect.top;
  if (currentShape === 'brush' || currentShape === 'freehand') {
    ctx.lineTo(currentX, currentY);
    ctx.strokeStyle = currentColor;
    ctx.lineWidth = brushSize;
    ctx.stroke();
  }
}

function endDrawing(e) {
  if (!isDrawing) return;
  isDrawing = false;
  undoStack.push(ctx.getImageData(0, 0, canvas.width, canvas.height));
}

function undoLastAction() {
  if (undoStack.length > 0) {
    redoStack.push(ctx.getImageData(0, 0, canvas.width, canvas.height));
    ctx.putImageData(undoStack.pop(), 0, 0);
  }
}

function redoLastAction() {
  if (redoStack.length > 0) {
    undoStack.push(ctx.getImageData(0, 0, canvas.width, canvas.height));
    ctx.putImageData(redoStack.pop(), 0, 0);
  }
}